VCPKG_PACKAGES = $$(VCPKG_ROOT)/packages

VCPKG_PACKAGE_LIST = $$files($${VCPKG_PACKAGES}/*)

VCPKG_USED_PACKAGES =


arch =
equals(QT_ARCH, x86_64) {
    arch = x64
} else {
    arch = x86
}

os = windows

platform = $${arch}-$${os}

LIBS += Shell32.lib

if (Debug|Release) {
    vcpkg_bins =
    vcpkg_libs =
    for (package_dir, VCPKG_PACKAGE_LIST) {
        INCLUDEPATH += $${package_dir}/include

        package_dirname = $$basename(package_dir)

        Debug {
            package_dir = $${package_dir}/debug
        }

        contains(package_dirname, .*$${platform}) {
            package_bin_dir = $${package_dir}/bin
            package_lib_dir = $${package_dir}/lib
            package_mlib_dir = $${package_lib_dir}/manual-link

            LIBS += -L$${package_bin_dir}
            LIBS += -L$${package_lib_dir}
            LIBS += -L$${package_mlib_dir}
        }
    }

    LIBS += -L$${_PRO_FILE_PWD_}
}

defineReplace(copyFiles) {
    args = $$split(1)
    filenames = $$member(args, 0, $$num_add($$size(args), -2))
    destination = $$last(args)

    for (filename, filenames) {
        command = copy "$$shell_path($${filename})" "$$shell_path($${destination})"
        system($$command)
    }
    return(1)
}

defineReplace(addVcpkgPackages) {
    isEmpty(VCPKG_DEPLOY_DIR) {
        isEmpty(DESTDIR) {
            Release:VCPKG_DEPLOY_DIR = $$OUT_PWD/release/
            Debug:VCPKG_DEPLOY_DIR = $$OUT_PWD/debug/
        } else {
            VCPKG_DEPLOY_DIR = $$DESTDIR
        }
    }

    if (Debug|Release) {
        vcpkg_bins =
        vcpkg_libs =

        entries = $$split($$lower(1))
        for (entry, entries) {
            args = $$split(entry, @)
            package_name = $$first(args)
            package_dirname = $${package_name}_$${platform}

            args = $$member(args, 1, $$num_add($$size(args), -1))

            package_dir = $${VCPKG_PACKAGES}/$${package_dirname}

            package_descriptor_file = $${package_dir}/CONTROL


            Debug {
                package_dir = $${package_dir}/debug
            }

            package_bin_dir = $${package_dir}/bin
            package_lib_dir = $${package_dir}/lib

            package_bin_list = $$files($${package_bin_dir}/*.dll)
            package_lib_list = $$files($${package_lib_dir}/*.lib)

            package_descriptor = $$cat($$package_descriptor_file, lines)

            regex = "Depends:.*"
            package_dependencies = $$find(package_descriptor, $$quote($$regex))

            space = " "
            package_dependencies = $$replace(package_dependencies, Depends:,)
            package_dependencies = $$replace(package_dependencies, " ",)

            sep = ,
            deps = $$split(package_dependencies, ",")

            VCPKG_USED_PACKAGES *= $$package_name

            dep_options =
            new_deps =
            for (dep, deps) {
                !contains(VCPKG_USED_PACKAGES, $$dep) {
                    VCPKG_USED_PACKAGES += $$dep
                    new_deps += $$dep
                    dep_options += $${dep}@$$join(args, @)
                }
            }

            export(VCPKG_USED_PACKAGES)

            !isEmpty(dep_options) {
                $$addVcpkgPackages($$dep_options)
            }

            option_noautodeploy =
            option_noautolink =
            for (arg, args) {
                flag_deploy = noautodeploy no-auto-deploy
                contains(flag_deploy, $$arg) {
                    option_noautodeploy = arg
                }
                flag_link = noautolink no-auto-link
                contains(flag_link, $$arg) {
                    option_noautolink = arg
                }
            }

            isEmpty(option_noautodeploy) {
                for (package_bin, package_bin_list) {
                    vcpkg_bins += $${package_bin}
                }
            }

            isEmpty(option_noautolink) {
                for (package_lib, package_lib_list) {
                    vcpkg_libs += $${package_lib}
                }
            }
        }

        LIBS = $$vcpkg_libs $$LIBS

        !isEmpty(VCPKG_DEPLOY_DIR) {
            $$copyFiles($$vcpkg_bins $$VCPKG_DEPLOY_DIR)
        }

        export(LIBS)
    }
    return(1)
}
