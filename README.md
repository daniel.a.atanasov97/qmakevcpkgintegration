Adds support for linking and deploying vckpg installed packages
The VCPKG_ROOT variable must be defined as the vcpkg's git root
(the folder that contains vcpkg.exe) before including the .pri
I prefer using an environment variable since it isn't something that I change

Packages may be added via the addVcpkgPackages replace function:
    $$addVcpkgPackages(package1@option1@option2 package2@option1@option2)

Package names are the same as in vcpkg
Currenly, the only options are the following:
    - no-auto-link (or noautolink) - disables linking towards the package
    - no-auto-deploy (or noautodeploy) - disables deploying of shared libraries

Both are enabled by default for every package
If you are using qt then you will have to mark qt5-base as no-auto-link

The deployment destination can be controlled via the VCPKG_DEPLOY_DIR variable
If it isn't defined then DESTDIR is used, and if it isn't defined either then
it deploys to $$OUT_PWD/release/ and $$OUT_PWD/debug/

Example (assuming VCPKG_QT_PRI points to the .pri):
    include($$(VCPKG_QT_PRI))
    Release:VCPKG_DEPLOY_DIR = $$OUT_PWD/bin/release/
    Debug:VCPKG_DEPLOY_DIR = $$OUT_PWD/bin/debug/
    $$addVcpkgPackages(qt5-base@no-auto-link qscintilla yaml-cpp@no-auto-deploy)

This will do the following:
    - deploy but not link against qt5-base
    - deploy and link against qscintilla
    - not deploy but link against yaml-cpp
The packages will be deployed in $$OUT_PWD/bin/configuration/ for debug/release
respectively

Currently, deploying/linking against a package means deploying/linking
everything in it
This is the main difference to the builtin Visual Studio integration

Library directories are added to LIBS even if you don't add the package
This is so you can manually link against it if needed